'use strict';
/**
 * @ngdoc directive
 * @name f1.components.timeline
 * @scope
 * @restrict AE
 *
 * @description
 * An overview of season winners
 */

;(function() {
    angular.module('f1.components.timeline', ['f1.services.erdgast'])
    .directive('f1Timeline', f1Timeline);

    f1Timeline.$inject = ['$timeout', 'erdgast'];

    function f1Timeline($timeout, erdgast) {
        var directive = {
            link: link,
            scope: true,
            restrict: 'AE',
            templateUrl: 'components/timeline/templates/timeline.html'
        };

        return directive;

        function link($scope, element, attr) {

            erdgast.season.list(2005, 2015)
                .then(proccesSeasons);

            $scope.toggleSeason = toggleSeason;

            $scope.isSeasonWinner = isSeasonWinner;

            function proccesSeasons(result){
                $scope.seasons = result;
            }

            function toggleSeason(season){

                if(!season.open){
                    season.open = true;
                    season.animate = false;

                    $timeout(function(){
                        season.animate = true;
                    },100);
                    $timeout(function(){
                        season.animationFinished = true;
                    },600);
                }
                else{
                    season.animationFinished = false;

                    $timeout(function(){
                        season.animate = false;
                    },100);
                    $timeout(function(){
                        season.open = false;
                    },600);
                }

                if(!season.races){
                    loadSeasonRaces(season);
                }
            }

            function loadSeasonRaces(season){
                season.loading = true;
                season.races = [];

                erdgast.races.list(season.season).then(function processRaces(result){
                    season.loading = false;
                    season.races = result;
                });
            }

            function isSeasonWinner(race, season){
                return season.DriverStandings[0].Driver.code == race.Results[0].Driver.code;
            }
        }
    }
})();