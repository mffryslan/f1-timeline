'use strict';

// Declare app level module which depends on views, and components
angular.module('f1', [
  'f1.components.timeline'
]);