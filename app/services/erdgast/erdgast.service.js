'use strict';

;(function () {
    angular.module('f1.services.erdgast', [])
        .factory('erdgast', erdgastService);

    erdgastService.$inject = ['$http',];

    function erdgastService($http) {
        var START_SEASON = 1950;
        var API_URL = 'http://ergast.com/api/f1';

        return {
            season: {
                list: listSeasons
            },
            races: {
                list: listRaces
            }
        };

        function listSeasons(startYear, endYear) {
            return $http({
                method: 'GET',
                url: API_URL + '/driverStandings/1.json?offset=' + (startYear - START_SEASON) + '&limit=' + (endYear - startYear + 1)
            }).then(function parseResult(result) {
                return result.data.MRData.StandingsTable.StandingsLists.sort(function sortSeasons(season1, season2){
                    return parseInt(season1.season) > parseInt(season2.season) ? -1 : 1;
                });
            });
        }

        function listRaces(year) {
            return $http({
                method: 'GET',
                url: API_URL + '/' + year + '/results/1.json'
            }).then(function parseResult(result) {
                return result.data.MRData.RaceTable.Races.sort(function sortRaces(race1, race2){
                    return parseInt(race1.date.replace('-','')) > parseInt(race2.date.replace('-','')) ? -1 : 1;
                });
            });
        }
    }
})();