# F1-Timeline

This web app sole purpose is an assessment for an potential front-end developer position at Backbase.

## Aproach

First step was defining the scope of the project. Then I started on a wireframe in Sketch.
As normally you would work with an UX expert, the design has been kept as simple as possible.
Formula 1 primarly uses black, red and white as color scheme, so these were re-used in the project.

Second was reading up on the pre-defined API, which could do *bleep* lot more then just showing these results.
Keeping to the scope, I copied the angular-seed project and started on a service and a minimal implementation.

Third was polishing with some CSS. Normally, I would use Sass or Less, but as the project is so small the
overhead (adding compile time) would outway the benefits (clean code). The same goes for libraries like bootstrap.

The fourth step would be to clean up all the code, add comments, unit tests and e2e (protractor tests), but household
called me away from the last 2 hours of this project. Sorry!

Total time spent: 4 hours

## Installation

### 1. Install Dependencies

```
npm install
```

### 2. Run the Application

```
npm start
```

Now browse to the app at `http://localhost:8000/index.html`.
